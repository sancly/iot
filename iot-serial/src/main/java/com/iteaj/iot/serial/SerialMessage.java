package com.iteaj.iot.serial;

import com.iteaj.iot.client.ClientMessage;

public class SerialMessage extends ClientMessage {

    public SerialMessage(byte[] message) {
        super(message);
    }

    public SerialMessage(MessageHead head) {
        super(head);
    }

    public SerialMessage(MessageHead head, MessageBody body) {
        super(head, body);
    }

    @Override
    protected MessageHead doBuild(byte[] message) {
        return null;
    }
}
