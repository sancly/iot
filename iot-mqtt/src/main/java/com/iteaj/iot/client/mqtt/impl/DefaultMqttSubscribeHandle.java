package com.iteaj.iot.client.mqtt.impl;

import com.iteaj.iot.client.ClientConnectProperties;
import com.iteaj.iot.client.ClientProtocolHandle;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.CollectionUtils;

import java.util.List;

public class DefaultMqttSubscribeHandle implements ClientProtocolHandle<DefaultMqttSubscribeProtocol> {

    private MqttSubscribeListenerManager listenerManager;
    private Logger logger = LoggerFactory.getLogger(getClass());

    public DefaultMqttSubscribeHandle(MqttSubscribeListenerManager listenerManager) {
        this.listenerManager = listenerManager;
    }

    @Override
    public Object handle(DefaultMqttSubscribeProtocol protocol) {
        try {
            // 先校验客户端是否绑定监听器
            DefaultMqttConnectProperties properties = protocol.requestMessage().getProperties();
            if(properties.getListener() != null) {
                properties.getListener().onSubscribe(protocol); return null;
            }

            // 匹配全局监听器
            List<MqttSubscribeListener> listeners = listenerManager.matcher(protocol.getTopic(), properties);
            if(!CollectionUtils.isEmpty(listeners)) {
                listeners.forEach(listener -> listener.onSubscribe(protocol)); // 执行所有已经匹配的监听
            } else {
                logger.warn("客户端(MQTT<默认>) 未找到匹配的[MqttSubscribeListener] - topic：{} - qos：{}", protocol.getTopic(), protocol.getQoS());
            }
        } catch (Exception e) {
            logger.error("客户端(MQTT<默认>) 事件处理异常 - topic：{} - qos：{}", protocol.getTopic(), protocol.getQoS(), e);
        }

        return null;
    }
}
