package com.iteaj.iot.client.mqtt.gateway;

import com.alibaba.fastjson.JSONObject;
import com.iteaj.iot.client.mqtt.MqttClientException;
import com.iteaj.iot.client.mqtt.gateway.adapter.MqttGatewayByteHandle;
import com.iteaj.iot.client.mqtt.gateway.adapter.MqttGatewayJsonHandle;
import com.iteaj.iot.client.mqtt.message.MqttMessageHead;
import com.iteaj.iot.handle.proxy.ProtocolHandleInvocationHandler;
import com.iteaj.iot.handle.proxy.ProtocolHandleProxy;
import com.iteaj.iot.handle.proxy.ProtocolHandleProxyMatcher;
import com.iteaj.iot.message.DefaultMessageBody;
import io.netty.handler.codec.mqtt.MqttQoS;
import org.springframework.util.StringUtils;

public class MqttGatewayProxyMatcher implements ProtocolHandleProxyMatcher {

    @Override
    public boolean matcher(Object target) {
        return target instanceof MqttGatewayHandle;
    }

    @Override
    public ProtocolHandleInvocationHandler invocationHandler(Object target, ProtocolHandleProxy handleProxy) {
        return new ProtocolHandleInvocationHandler(target, handleProxy) {

            @Override
            public Class<MqttGatewayHandle> getProxyClass() {
                return MqttGatewayHandle.class;
            }

            @Override
            protected Object proxyHandle(Object value, Object proxy) {
                final MqttGatewayConnectProperties properties = getOriTarget().getProperties(value);
                if(properties == null) {
                    throw new MqttClientException("请返回正确的mqtt配置[MqttGatewayConnectProperties]");
                }
                final MqttQoS qoS = properties.getQoS();
                final String topic = properties.getTopic();
                final MqttGatewayHead mqttMessageHead = getOriTarget().getMqttGatewayHead(value);
                final MqttGatewayMessage gatewayMessage = new MqttGatewayMessage(mqttMessageHead, qoS, topic);

                /**
                 * 发布报文
                 */
                if(proxy instanceof MqttGatewayJsonHandle) {
                    final byte[] jsonBytes = JSONObject.toJSONBytes(value);
                    gatewayMessage.setBody(new DefaultMessageBody(jsonBytes));
                } else if(proxy instanceof MqttGatewayByteHandle) {
                    gatewayMessage.setBody(new DefaultMessageBody((byte[]) value));
                } else {
                    throw new MqttClientException("不支持的处理器["+getTarget().getClass().getSimpleName()+"]");
                }

                if(!StringUtils.hasText(topic)) {
                    throw new MqttClientException("未指定Mqtt要发布的topic在配置[MqttGatewayConnectProperties]");
                }

                if(logger.isTraceEnabled()) {
                    logger.trace("MQTT网关代理 发送请求 - 请求配置: {} - 报文: {}", properties, gatewayMessage);
                }

                // 发送请求
                new MqttGatewayProtocol(gatewayMessage).request(properties);
                return value;
            }

            @Override
            public MqttGatewayHandle getTarget() {
                return (MqttGatewayHandle) super.getTarget();
            }

            @Override
            public MqttGatewayHandle getOriTarget() {
                return (MqttGatewayHandle) super.getOriTarget();
            }
        };
    }

}
