package com.iteaj.iot.plc.omron;

import com.iteaj.iot.ProtocolType;
import com.iteaj.iot.client.ClientConnectProperties;
import com.iteaj.iot.client.TcpSocketClient;
import com.iteaj.iot.client.component.TcpClientComponent;
import com.iteaj.iot.client.protocol.ServerInitiativeProtocol;

/**
 * 欧姆龙PLC编解码组件
 */
public class OmronComponent extends TcpClientComponent<OmronMessage> {

    public OmronComponent() { }

    public OmronComponent(OmronConnectProperties config) {
        super(config);
    }

    @Override
    public TcpSocketClient createNewClient(ClientConnectProperties config) {
        return new OmronTcpClient(this, (OmronConnectProperties) config);
    }

    @Override
    public String getName() {
        return "欧姆龙PLC";
    }

    @Override
    public String getDesc() {
        return getName();
    }

    @Override
    public Class<OmronMessage> getMessageClass() {
        return OmronMessage.class;
    }

    /**
     * 欧姆龙plc的协议都是客户端主动协议
     * @param message
     * @param type
     * @return
     */
    @Override
    protected ServerInitiativeProtocol instanceProtocol(OmronMessage message, ProtocolType type) {
        return null;
    }
}
