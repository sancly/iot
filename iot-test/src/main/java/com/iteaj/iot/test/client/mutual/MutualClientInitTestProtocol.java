package com.iteaj.iot.test.client.mutual;

import com.iteaj.iot.ProtocolType;
import com.iteaj.iot.client.protocol.ClientInitiativeProtocol;
import com.iteaj.iot.message.DefaultMessageHead;
import com.iteaj.iot.test.IotTestProperties;
import com.iteaj.iot.utils.ByteUtil;

import java.nio.charset.StandardCharsets;

public class MutualClientInitTestProtocol extends ClientInitiativeProtocol<MutualClientMessage> {

    private String text;
    private String deviceSn;
    private MutualType type;

    public MutualClientInitTestProtocol(String deviceSn, String text) {
        this.text = text;
        this.deviceSn = deviceSn;
    }

    @Override
    protected MutualClientMessage doBuildRequestMessage() {
        IotTestProperties.MutualConnectProperties config = (IotTestProperties.MutualConnectProperties) getIotClient().getConfig();
        this.type = config.getType();
        DefaultMessageHead head = new DefaultMessageHead(this.deviceSn, null, null);
        if(type == MutualType.HEX) {
            head.setMessage(ByteUtil.hexToByte(text));
        } else if(type == MutualType.UTF8) {
            head.setMessage(text.getBytes(StandardCharsets.UTF_8));
        } else {
            head.setMessage(text.getBytes(StandardCharsets.US_ASCII));
        }

        return new MutualClientMessage(head);
    }

    @Override
    public void doBuildResponseMessage(MutualClientMessage responseMessage) { }

    @Override
    public ProtocolType protocolType() {
        return null;
    }

}
