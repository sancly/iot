package com.iteaj.iot.test.taos;

import com.iteaj.iot.taos.STable;
import com.iteaj.iot.tools.annotation.TagsResolver;
import org.springframework.stereotype.Component;

/**
 * 用来解析 Tags值
 * @see STable#tagsResolver()
 * @see TaosBreakerUsingStable
 */
@Component
public class TaosTagResolver implements TagsResolver {

    private String[] tags = new String[] {"FJ.XM", "FJ.QZ", "FJ.ZZ", "FJ.LY", "FJ.SM", "FJ.FZ", "QZ.AX", "BeiJing", "ShangHai", "ShenZhen"};

    @Override
    public Object resolve(String tableName, String tagName) {
        String index = tableName.substring(tableName.length() - 1);
        return tags[Integer.valueOf(index)];
    }
}
