package com.iteaj.iot.test.server.breaker;

import com.iteaj.iot.AbstractProtocol;
import com.iteaj.iot.ProtocolType;
import com.iteaj.iot.config.ConnectProperties;
import com.iteaj.iot.server.component.LengthFieldBasedFrameDecoderServerComponent;
import com.iteaj.iot.server.protocol.ClientInitiativeProtocol;
import com.iteaj.iot.test.BreakerProtocolType;

import java.nio.ByteOrder;

public class BreakerServerComponent extends LengthFieldBasedFrameDecoderServerComponent<BreakerServerMessage> {

    public BreakerServerComponent(ConnectProperties connectProperties) {
        super(connectProperties, ByteOrder.LITTLE_ENDIAN, 256, 0
                , 4, 0, 0, true);
    }

    @Override
    public String getDesc() {
        return "断路器设备服务端";
    }

    @Override
    public AbstractProtocol getProtocol(BreakerServerMessage message) {
        BreakerProtocolType type = message.getHead().getType();

        if(type == BreakerProtocolType.PushData) {
            return new DataAcceptProtocol(message);
        }

        return null;
    }

    @Override
    protected ClientInitiativeProtocol<BreakerServerMessage> instanceProtocol(BreakerServerMessage message, ProtocolType type) {
        return null;
    }

    @Override
    public String getName() {
        return "用来对接断路器设备";
    }
}
