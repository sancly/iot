package com.iteaj.iot.test.client.breaker;

import com.iteaj.iot.client.ClientComponent;
import com.iteaj.iot.client.IotClientBootstrap;
import com.iteaj.iot.client.protocol.ClientInitiativeProtocol;
import com.iteaj.iot.message.DefaultMessageBody;
import com.iteaj.iot.message.DefaultMessageHead;
import com.iteaj.iot.test.BreakerProtocolType;
import com.iteaj.iot.test.MessageCreator;
import com.iteaj.iot.test.StatusCode;
import com.iteaj.iot.test.TestConst;
import com.iteaj.iot.utils.ByteUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 数据推送协议
 */
public class DataPushProtocol extends ClientInitiativeProtocol<BreakerClientMessage> {

    private String deviceSn;

    private static Logger logger = LoggerFactory.getLogger(DataPushProtocol.class);

    public DataPushProtocol(String deviceSn) {
        this.deviceSn = deviceSn;
    }

    @Override
    protected BreakerClientMessage doBuildRequestMessage() {
        DefaultMessageBody messageBody = MessageCreator.buildBreakerBody();

        DefaultMessageHead messageHead = MessageCreator.buildBreakerHeader(Long
                .valueOf(this.deviceSn), messageBody.getLength(), this.protocolType());

        return new BreakerClientMessage(messageHead, messageBody);
    }

    @Override
    public void doBuildResponseMessage(BreakerClientMessage responseMessage) {
        byte[] message = responseMessage.getMessage();
        int status = ByteUtil.bytesToInt(message, message.length - 4);
        StatusCode code = StatusCode.getInstance(status);
        if(code != StatusCode.Success) {
            ClientComponent component = IotClientBootstrap.getClientComponent(BreakerClientMessage.class);
            logger.error(TestConst.LOGGER_PROTOCOL_DESC, component.getName(), protocolType().desc, getEquipCode(), getMessageId(), "不通过("+code.desc+")");
        }
    }

    @Override
    public BreakerProtocolType protocolType() {
        return BreakerProtocolType.PushData;
    }
}
