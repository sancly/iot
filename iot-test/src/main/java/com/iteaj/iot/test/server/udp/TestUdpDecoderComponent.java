package com.iteaj.iot.test.server.udp;

import com.iteaj.iot.AbstractProtocol;
import com.iteaj.iot.ProtocolType;
import com.iteaj.iot.server.component.DatagramPacketDecoderServerComponent;
import com.iteaj.iot.server.protocol.ClientInitiativeProtocol;
import com.iteaj.iot.test.IotTestProperties;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Component;

/**
 * create time: 2021/9/12
 *
 * @author iteaj
 * @since 1.0
 */
@Component
@ConditionalOnProperty(prefix = "iot.test", name = "udp.start", havingValue = "true")
public class TestUdpDecoderComponent extends DatagramPacketDecoderServerComponent<UdpTestMessage> {

    public TestUdpDecoderComponent(IotTestProperties properties) {
        super(properties.getUdp());
    }

    @Override
    public String getDesc() {
        return "测试UDP协议";
    }

    @Override
    public AbstractProtocol getProtocol(UdpTestMessage message) {
        return new UdpTestClientProtocol(message);
    }

    @Override
    public String getName() {
        return "UDP";
    }

    @Override
    protected ClientInitiativeProtocol<UdpTestMessage> instanceProtocol(UdpTestMessage message, ProtocolType type) {
        return null;
    }

    @Override
    public UdpTestMessage createMessage(byte[] message) {
        return new UdpTestMessage(message);
    }
}
