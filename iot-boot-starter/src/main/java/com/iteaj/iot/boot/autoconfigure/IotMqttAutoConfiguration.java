package com.iteaj.iot.boot.autoconfigure;

import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.context.annotation.Configuration;

@Configuration(proxyBeanMethods = false)
@ConditionalOnClass(name = "com.iteaj.iot.client.mqtt.IotMqttAutoConfiguration")
public class IotMqttAutoConfiguration {

    @Configuration
    public static class IotMqttConfiguration extends com.iteaj.iot.client.mqtt.IotMqttAutoConfiguration { }
}
