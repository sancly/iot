package com.iteaj.iot.boot.autoconfigure;

import com.iteaj.iot.IotCoreProperties;
import com.iteaj.iot.IotThreadManager;
import com.iteaj.iot.server.IotServeBootstrap;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Configuration;

@Configuration(proxyBeanMethods = false)
@ConditionalOnClass(name = "com.iteaj.iot.server.IotServeBootstrap")
public class ServerAutoConfiguration {

    @Configuration
    public static class IotServerAutoConfiguration extends IotServeBootstrap {

        public IotServerAutoConfiguration(ApplicationContext applicationContext
                , IotThreadManager threadManager, IotCoreProperties coreProperties) {
            super(applicationContext, threadManager, coreProperties);
        }
    }
}
