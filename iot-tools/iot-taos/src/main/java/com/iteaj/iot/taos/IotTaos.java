package com.iteaj.iot.taos;

import org.springframework.jdbc.core.JdbcTemplate;

/**
 * Taos物联网分布式数据库适配
 */
public interface IotTaos {

    DefaultTaosDatabaseSource databaseSource = new DefaultTaosDatabaseSource();

    /**
     * 使用的jdbc模板, 用来支持多Taos数据源
     * @return jdbcTemplate
     */
    default JdbcTemplate taosJdbcTemplate(Object entity) {
        return databaseSource.taosJdbcTemplate;
    }

    /**
     * 默认的taos jdbc数据源
     */
    class DefaultTaosDatabaseSource {
        public JdbcTemplate taosJdbcTemplate;
    }
}
