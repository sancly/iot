package com.iteaj.iot.simulator;

import com.iteaj.iot.client.ClientConnectProperties;
import com.iteaj.iot.consts.MessageFormat;

/**
 * 模拟器连接配置
 */
public abstract class SimulatorConnectProperties extends ClientConnectProperties {

    private MessageFormat format;
    private SimulatorProtocolType type;

    public SimulatorConnectProperties(String remoteHost, Integer remotePort, SimulatorProtocolType type, String connectKey) {
        super(remoteHost, remotePort, connectKey);
        this.type = type;
        this.format = MessageFormat.Ascii;
    }

    /**
     * @see SimulatorClientComponent#getProtocol(SimulatorClientMessage)
     * @param message
     * @return
     */
    public abstract SimulatorProtocol getProtocol(SimulatorClientMessage message);

    public MessageFormat getFormat() {
        return format;
    }

    public SimulatorConnectProperties setFormat(MessageFormat format) {
        this.format = format; return this;
    }

    public SimulatorProtocolType getType() {
        return type;
    }

    public void setType(SimulatorProtocolType type) {
        this.type = type;
    }
}
